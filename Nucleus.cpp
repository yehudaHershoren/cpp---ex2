#include "Nucleus.h"
#include <algorithm>

/*
	the function initializes a Gene
	input: the start index of the gene, end index and if the gene is on the complementary dna strand
	output: none
*/
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

//getters of Gene
unsigned int Gene::get_start() const
{
	return this->_start;
}

unsigned int Gene::get_end() const
{
	return this->_end;
}

bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}

//setters of Gene

void Gene::set_start(const unsigned int new_start) 
{
	this->_start = new_start;
}


void Gene::set_end(const unsigned int new_end)
{
	this->_end = new_end;
}

void Gene::set_on_complementary_dna_strand(const bool new_on_complementary)
{
	this->_on_complementary_dna_strand = new_on_complementary;
}


/*
	the function finds the complementary nucleotide of another nucleotide
	input: a nucleotide
	output: the nucleotide's complementary
*/
char Nucleus::get_complementary_nucleotide(const char nuc)
{
	char result = 0;

	switch (nuc)
	{
	case 'A':
		result = 'T';
		break;

	case 'T':
		result = 'A';
		break;

	case 'G':
		result = 'C';
		break;

	case 'C':
		result = 'G';
		break;
	default:
		break;
	}

	return result;
}

/*
	the function initializes a Nucleus
	input:	dna_sequence - a main dna sequence
	output: none
*/
void Nucleus::init(const string dna_sequence)
{
	int i = 0;
	string complementary = "";

	for (i = 0; i < dna_sequence.length(); i++) 
	{
		//making sure dna is valid:
		if (dna_sequence[i] != 'A' && dna_sequence[i] != 'T' && dna_sequence[i] != 'G' && dna_sequence[i] != 'C')
		{
			std::cerr << "invalid DNA!";
			_exit(1);
		}
		else //creating complementary dna strand
		{
			complementary += get_complementary_nucleotide(dna_sequence[i]); // adding to the complementary strand the complenetary nucleotide
		}
	}

	this->_DNA_strand = dna_sequence;
	this->_complementary_DNA_strand = complementary;
}

/*
	the function creates a rna transcript of a certain gene
	input:	gene - the gene we want to create a rna transcript from
	output: the rna transcript
*/
string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	string strand = "";
	string gene_sequence = "";
	string rna = "";

	if (gene.is_on_complementary_dna_strand())
	{
		strand = this->_complementary_DNA_strand;
	}
	else
	{
		strand = this->_DNA_strand;
	}
	gene_sequence = strand.substr(gene.get_start(), gene.get_end() - gene.get_start() + 1); //substringing the gene from the strand
	rna = gene_sequence;
	std::replace(rna.begin(), rna.end(), 'T','U'); // replacing all T to U


	return rna;
}

/*
	the function returns a reversed copy of the main dna strand
	input:none
	output: the reversed dna strand
*/
string Nucleus::get_reversed_DNA_strand() const
{
	string copy = this->_DNA_strand;
	reverse(copy.begin(), copy.end()); //using the function reverse() from 'algorithm' libary
	return copy;
}

/*
	the function finds how many occurrences of a codon are in the main dna strand
	input: the codon
	output: occurrences of the codon in the main dna strand
*/
unsigned int Nucleus::get_num_of_codon_appearances(const string& codon) const
{
	unsigned int occurrences = 0;
	int position = 0;

	while (this->_DNA_strand.find(codon, position) < this->_DNA_strand.length()) //while we still find occurrences
	{
		occurrences++;
		position += codon.length(); //moving the starting search position as the the length of the codon
	}

	return occurrences;
}

//get & set functions for _DNA_strand: (for bonus)
void Nucleus::set_dna_strand(const string dna_strand)
{
	this->_DNA_strand = dna_strand;
}

string Nucleus::get_dna_strand() const
{
	return this->_DNA_strand;
}

