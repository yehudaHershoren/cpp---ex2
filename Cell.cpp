#include "Cell.h"


/*
	the function initializes a new cell
	input:	dna_sequence - the main DNA sequence
			glocuse_receptor_gene - the gene that creates the glocuse receptor
*/
void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_nucleus.init(dna_sequence);
	this->_glocus_receptor_gene = glucose_receptor_gene;
	this->_mitochondrion.init();
	this->_atp_untis = 0;
}

/*
	the function loads the cell with energy
	input: none
	output: true if load succeed, false else
*/
bool Cell::get_ATP()
{
	bool result = 0;

	string rna = this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene); //getting the RNA transcript
	Protein* protein = this->_ribosome.create_protein(rna); //trying to create the protein

	if (protein == nullptr) //creation of protein failed
	{
		std::cerr << "creation of protien failed!";
		_exit(1);
	}
	
	this->_mitochondrion.insert_glucose_receptor(*protein); //sending the protein to the mitochondrion
	this->_mitochondrion.set_glucose(MIN_GLOCUSE_ATP); //setting glocuse level to minimum

	if (!this->_mitochondrion.produceATP()) // can't produce atp
	{
		result = false;
	}
	else
	{
		this->_atp_untis = PRODUCE_ATP;
		result = true;
	}
}

// get function for _nucleus (for bonus)
Nucleus* Cell::get_nucleus()
{
	return &(this->_nucleus);
}
