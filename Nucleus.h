#pragma once

#include <iostream> 
#include <string> 

using std::string;

class Gene
{
private:
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;
public:
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);

	//getters & setters
	unsigned int get_start() const;
	unsigned int get_end() const;
	bool is_on_complementary_dna_strand() const;

	void set_start(const unsigned int new_start);
	void set_end(const unsigned int new_end);
	void set_on_complementary_dna_strand(const bool new_on_complementary);
};

class Nucleus
{
private:
	string _DNA_strand;
	string _complementary_DNA_strand;

	char get_complementary_nucleotide(const char nuc);

public:
	void init(const string dna_sequence);
	string get_RNA_transcript(const Gene& gene) const;
	string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codon_appearances(const string& codon) const;
	
	//for bonus:
	void set_dna_strand(const string dna_strand);
	string get_dna_strand() const;
};