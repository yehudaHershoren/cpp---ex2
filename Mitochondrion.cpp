#include "Mitochondrion.h"

/*
	the function initializes a new Mitochondrion
	input & output: none
*/
void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}

/*
	the function tries to insert a glocus receptor according to a protein
	if the aminoacid sequence is valid - the glocus receptor will be inserted
	input: the protein
	outout: none

*/
void Mitochondrion::insert_glucose_receptor(const Protein & protein)
{
	bool validSequence = true;
	int i = 0;
	AminoAcid protein_sequence[RECEPTOR_LEN] = { PROTEIN_GLOCUS_RECEPTOR }; //array of the sequence
	AminoAcidNode* curr = protein.get_first();

	while (curr != NULL && validSequence)
	{
		if (curr->get_data() != protein_sequence[i]) //if the sequence is incorrect (amino acid don't match)
		{
			validSequence = false;
		}
		curr = curr->get_next(); // moving to the next anino acid
		i++;
	}

	if (validSequence) 
	{
		this->_has_glocuse_receptor = true;
	}
	
}

/*
	the function sets a new value for _glovus_level
	input: the new value
	output: none
*/
void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}

/*
	the function checks if the Mitochondrion can produce atp
	input: the glocuse unti (we already have it..?)
	output:	true if the mitochondrion can produce atp, false else.
*/
bool Mitochondrion::produceATP() const
{
	return (this->_glocuse_level >= MIN_GLOCUSE_ATP && this->_has_glocuse_receptor);
}
