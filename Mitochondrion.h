#pragma once
#include "Protein.h"

#define PROTEIN_GLOCUS_RECEPTOR ALANINE, LEUCINE, GLYCINE, HISTIDINE, LEUCINE, PHENYLALANINE, AMINO_CHAIN_END
#define RECEPTOR_LEN 7

#define MIN_GLOCUSE_ATP 50

class Mitochondrion
{
private:
	unsigned int _glocuse_level;
	bool _has_glocuse_receptor;

public:
	void init();
	void insert_glucose_receptor(const Protein & protein);
	void set_glucose(const unsigned int glocuse_units);
	bool produceATP() const;

};