#pragma once
#include <iostream>
#include "Cell.h"

class Virus
{
private:
	std::string _RNA_sequence;
public:
	void init(const std::string RNA_sequence);
	void infect_cell(Cell& cell) const;
};