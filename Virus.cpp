#include "Virus.h"
#include <algorithm>

/*
	the function initializes a new Virus
	input: the RNA sequence of the virus
	output: none
*/
void Virus::init(const std::string RNA_sequence)
{
	this->_RNA_sequence = RNA_sequence;
}

/*
	the function infects a cell with the virus
	input: the cell to infect
	output: none
*/
void Virus::infect_cell(Cell& cell) const
{
	string dna_strand = "";
	string first_half = "";
	string second_half = "";
	string final_dna_virus = "";
	string insert_dna = this->_RNA_sequence; //creating the sequence we are going to insert

	std::replace(insert_dna.begin(), insert_dna.end(), 'U', 'T'); // switching all U to T

	dna_strand = cell.get_nucleus()->get_dna_strand(); //getting the main dna strand of the cell
	first_half = dna_strand.substr(0, dna_strand.length() / 2); //getting the first half of the dna strand
	second_half = dna_strand.substr(dna_strand.length() / 2 , dna_strand.length() - dna_strand.length() / 2); //getting the second half of the dna strand

	final_dna_virus = first_half + insert_dna + second_half; //creating the final virus dna sequence

	cell.get_nucleus()->init(final_dna_virus); //infecting
}


