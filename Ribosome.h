#pragma once
#include "Protein.h"

#define NUM_OF_NUC 3

class Ribosome
{
public:
	Protein* create_protein(std::string &RNA_transcript) const;
};
