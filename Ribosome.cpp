#include "Ribosome.h"
#include "AminoAcid.h"
#include <iostream>
using std::string;

/*
	the function creates a protein according to a RNA transcript
	input: the RNA transcript
	output: the created protein
*/
Protein* Ribosome::create_protein(std::string &RNA_transcript) const
{
	bool loop = true;
	string codon = "";
	AminoAcid amino_result = UNKNOWN;
	string copy_rna = "";
	Protein* newProtein = new Protein;
	
	newProtein->init();
	copy_rna = RNA_transcript;

	while (copy_rna.length() >= NUM_OF_NUC && loop)
	{
		codon = copy_rna.substr(0,NUM_OF_NUC); // getting the codon from the RNA
		std::cout << "codon = " << codon;
		copy_rna = copy_rna.substr(NUM_OF_NUC , copy_rna.length() - NUM_OF_NUC); //cutting the codon from the RNA
		amino_result = get_amino_acid(codon);

		if (amino_result == AminoAcid::UNKNOWN)
		{
			newProtein = nullptr;
			loop = false;
		}
		else
		{
			newProtein->add(amino_result);
		}

		
	}

	return newProtein;
}
